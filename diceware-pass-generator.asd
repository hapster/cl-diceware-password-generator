(asdf:defsystem #:hapster.diceware-pass-generator
  :version "0.0.1"
  :description "Generate a password of a specified length using the diceware method"
  :serial t
  :license "GPLv3"
  :components
  ((:file "packages")
   (:file "diceware-pass-generator" :depends-on ("packages")))
  :depends-on (:str))
